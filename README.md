# Tiny TPSM83100

Just a tiny DC/DC board using the TI's [TPSM83100](https://www.ti.com/product/TPSM83100) power module.

# Schematic

![](img/sch.jpg)

# PCB

![](img/pcb.jpg)
